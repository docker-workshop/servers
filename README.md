# Servers
---
SELA servers

username: docker

password: docker

| # | Name                   | External IP    |
|---|------------------------|----------------|
| 1 | kubernetes-course-00lz | 34.76.182.234  |
| 2 | kubernetes-course-r91l | 34.77.5.180    |
| 3 | kubernetes-course-sdsv | 35.195.103.245 |

# Google sheet list
---

https://docs.google.com/spreadsheets/d/1z8vDRAZd6rTGqZwD1O1w_b55L8nAcvCLtdA9DbqYobg/edit#gid=0
